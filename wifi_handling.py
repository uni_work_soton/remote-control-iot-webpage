import network
import secrets
import machine

ssid = secrets.secrets['ssid']
password = secrets.secrets['pass']

wlan = network.WLAN(network.STA_IF)
wlan.active(True)

#Taken from https://github.com/insighio/microCoAPy/blob/master/examples/esp/esp_wifi_coap_client.py
def connectToWiFi():
    nets = wlan.scan()
    for net in nets:
        ssid2 = net[0].decode("utf-8")
        if ssid2 == ssid:
            print('Network found!')
            wlan.connect(ssid, password)
            while not wlan.isconnected():
                machine.idle()  # save power while waiting
            print('WLAN connection succeeded!')
            break

    return wlan.isconnected()