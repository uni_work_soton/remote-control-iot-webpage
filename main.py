from wifi_handling import *
from umqtt.simple import MQTTClient
import time

led_on = False

#parts of https://peppe8o.com/download/micropython/mqtt/mttq_picow_subscriber.py were repurposed

connectToWiFi()

mqtt_user = secrets.secrets['mqtt_user']
mqtt_pass = secrets.secrets['mqtt_pass']

def mqtt_connect():
    client = MQTTClient('upy_board', '192.168.1.228', user=mqtt_user, password=mqtt_pass, keepalive=60)
    client.connect()
    return client

def connection_lost():
    time.sleep(2)
    machine.reset()
    
def callback(_, message):
    led = machine.Pin("LED")
    global led_on
    if led_on == True:
        led.low()
        led_on = False
    else:
        led.high()
        led_on = True

    
client = mqtt_connect()
client.set_callback(callback)
client.subscribe('/upy')

last_message_timer = time.time()
keep_alive_timer = 10

while True:
    try:
        client.check_msg()
        time.sleep(0.01)
        if (time.time() - last_message_timer) > keep_alive_timer:
              client.publish('/keep_alive', "Keep alive message pi pico")
              last_message_timer = time.time()

    except OSError as e:
        print(e)
        reconnect()
        pass

client.disconnect()