import paho.mqtt.client as mqtt
from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
from server_secrets import secrets
from queue import Queue
import json
import time

#flask stuff below here
app = Flask(__name__)
socketio = SocketIO(app)
temp_sensor_global_queue = Queue(1)

"""
https://stackoverflow.com/a/49241088 was used in part here
"""

@app.route('/', methods=['POST', 'GET'])
def root():
    print(request.method)
    return render_template('page.html')

@socketio.on('connect')
def connect():
    print('recieved connect on server side')

@socketio.on('connect_ack')
def connect_ack():
    print('recieved connect_ack on server side')
    plug_state = get_state('plug_state')
    upy_state = get_state('microcontroller')
    socketio.emit('states', {'states': {"plug": str(plug_state), "upy": str(upy_state)}})

@socketio.on('client_change_plug')
def handle_plug_change():
    print('plug change request from client')
    plug_pushed()

@socketio.on('poll_sensor')
def handle_sensor_poll():
    print('sensor polled')
    poll_sensor()

@socketio.on('client_change_microcontroller')
def handle_upy_change():
    print('upy change request from client')
    upy_pushed()


#mqtt stuff below here
#for when the server.py connects to broker, and only at the initial when server.py starts
def on_mqtt_connect(client, userdata, flags, rc):
    client.subscribe("/plug/#")
    client.subscribe("/sensors/#")

    #empty parameter returns the current setting of plug, 
    #so as we're reading through every message recieved by subscribing to /plug/#,
    #we're gonna see the result of this in there
    client.publish('/plug/cmnd/power', '')

def on_message(client, userdata, msg):
    if msg.topic != '/plug/STATE' and msg.topic != '/plug/SENSOR':
        print(msg.topic + " " + str(msg.payload))
    if msg.topic == '/sensors/temp_sensor':
        print('new sensor reading recieved on server!')
        if not temp_sensor_global_queue.full():
            temp_sensor_global_queue.put_nowait(msg.payload.decode('utf-8'))

#when a client wants to change the plug state
def plug_pushed():
    #mqtt message to change plug power
    client.publish('/plug/cmnd/power', "TOGGLE")

    plug_state = get_state('plug_state')
    plug_state = not plug_state
    update_state('plug_state', plug_state)
    #websocket message to tell all connected clients plug state has changed
    socketio.emit('plug_change')


def poll_sensor():
    if not temp_sensor_global_queue.empty():
        msg = temp_sensor_global_queue.get_nowait()
        socketio.emit("temp_sensor_update", {"temp_sensor": msg})

        
        #check if temp thresholds are met for automatic enabling of subscribers...
        if float(msg)>26.0:
            #turn warning light on regardless of user input
            light_state = get_state('microcontroller')
            if not light_state:
                upy_pushed()
        if float(msg)>29:
            #turn fan on regardless of user input
            fan_state = get_state('plug_state')
            if not fan_state:
                plug_pushed()
        
    

def upy_pushed():
    client.publish('/upy', "")

    upy_state = get_state('microcontroller')
    upy_state = not upy_state
    update_state('microcontroller', upy_state)
    socketio.emit('upy_change')


#interacts with state persistence to get states of subscribers
def get_states():
    with open('subscriber_state.json', 'r') as file:
        states = json.load(file)
        #convert all states to bools, as needed for program
        states = {key:value=="True" for (key, value) in states.items()}
        return states
    
def get_state(subscriber_name):
    states = get_states()
    return states[subscriber_name]

#updates state persistence with new value of a subscriber
def update_state(subscriber_name, new_state):
    states = get_states()
    
    states[subscriber_name] = new_state

    #convert all states to strings, as needed for json storage
    states = {key:str(value) for (key,value) in states.items()}

    with open('subscriber_state.json', 'w') as file:
        json.dump(states, file)




username = secrets['username']
password = secrets['password']

client = mqtt.Client('webserver')
client.username_pw_set(username, password)
client.on_connect = on_mqtt_connect
client.on_message = on_message
client.connect('192.168.1.228')

keep_alive_timer = 10

client.loop_start()
socketio.run(app, host='0.0.0.0')