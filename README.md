# Remote Control Iot Webpage

Made a web page and server that communicates with IoT devices over MQTT for automatic control based on sensor readings, as well as manual override. This was then applied to create a temperature control system for a room, where a fan was turned on based on user set preferences for temperature, or alternatively the fan can be turnt on wirelessly by the user via the web page. 

This project was made as a part of a third year module at my university, Advanced Computer Networks.
